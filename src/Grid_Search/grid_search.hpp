#ifndef __GRID_SEARCH_HPP__
#define __GRID_SEARCH_HPP__

/*----------------------------------------------------------------------------*/
/* Bibliotecas                                                                */
/*----------------------------------------------------------------------------*/

#include <vector>

/*----------------------------------------------------------------------------*/
/* DEFINICIONES                                                               */
/*----------------------------------------------------------------------------*/

using namespace std;

typedef vector<float> Genoma;

/*----------------------------------------------------------------------------*/
/* DECLARACIONES DE CLASES                                                    */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* CLASE       | GridSearch                                                   */
/*----------------------------------------------------------------------------*/
/* DESCRIPCIÓN | Clase para la metaheurística grid search                     */
/*----------------------------------------------------------------------------*/

class GridSearch {
    public:

        // Constructor
        GridSearch( float min, float max, float gra, unsigned int c, float gra_final, float offset ) : _min(min), _max(max), _gra_inicial(gra), _cps(c), _gra_final(gra_final), _offset(offset) {
          _mejor = Genoma(_cps);
        };

        // Ejecuta la metaheurística
        void run_grid();

        // Se obtiene la mejor parametrización
        Genoma &get_parametrizacion();

        int torneo(vector<Genoma> &participantes);

        void optimizar_param(Genoma & param);

        void print_genoma(Genoma & gen);

    private:

        float _min;    // Valor mínimo de cada parámetro
        float _max;    // Valor máximo de cada parámetro
        float _gra_inicial;    // Granularidad
        unsigned int _cps;    // Cantidad de genes del genoma

        //Para GRASP
        float _gra_final;
        float _offset;


        Genoma _mejor;

        // Paso recursivo de la metaheurística
        void run_rec( Genoma & actual, int k );

        // Determina si la parametrización actual es mejor que la mejor parametrización
        bool es_mejor( Genoma & actual );

        void run_rec_top(Genoma &actual, int k, vector <float> & rangos);

        bool le_gana(Genoma & genoma_a, Genoma & genoma_b);

        void asingar_top(Genoma nuevo_mejor);

        void print_top();

};

int max_pos(vector<int> & resultados);
int min_pos(vector<int> & resultados);


pair<int,int> competirDosGenomas(vector<float> & genomaA, vector<float> & genomaB);


#endif    /* __GRID_SEARCH_HPP__ */
