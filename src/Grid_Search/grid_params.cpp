/*----------------------------------------------------------------------------*/
/* BIBLIOTECAS                                                                */
/*----------------------------------------------------------------------------*/

#include "grid_search.hpp"
#include <iostream>
#include <cstdlib>
#include <chrono>

#define ya chrono::high_resolution_clock::now


using namespace std;
/*----------------------------------------------------------------------------*/
/* MAIN                                                                       */
/*----------------------------------------------------------------------------*/

int main( int argc, char **argv ) {
    // Obtengo los parámetros
    unsigned int c = std::atoi( argv[1] ); //cantidad de genes
    float gra = (float) std::atoi( argv[2] ); //granularidad
    unsigned int corridas = std::atoi( argv[3] ); //cantidad de corridas de las búsquedas locales
    unsigned int corridas_grasp = std::atoi( argv[4]); //cantidad de corridas de GRASP
    gra = gra/100;

    // Creo los candidatos iniciales

    //GridSearch gs_1 = GridSearch( -1, -0.75, gra, c, 0, 0);
    //GridSearch gs_2 = GridSearch( -0.75, -0.5, gra, c, 0, 0);
    //GridSearch gs_3 = GridSearch( -0.5, -0.25, gra, c, 0, 0);
    //GridSearch gs_4 = GridSearch( -0.25, 0, gra, c, 0, 0);
    //GridSearch gs_5 = GridSearch( -0, 0.25, gra, c, 0, 0);
    //GridSearch gs_6 = GridSearch( 0.25, 0.5, gra, c, 0, 0);
    //GridSearch gs_7 = GridSearch( 0.5, 0.75, gra, c, 0, 0);
    //GridSearch gs_8 = GridSearch( 0.75, 1, gra, c, 0, 0);
    //Genoma mejor_1(c);
    //Genoma mejor_2(c);
    //Genoma mejor_3(c);
    //Genoma mejor_4(c);
    //Genoma mejor_5(c);
    //Genoma mejor_6(c);
    //Genoma mejor_7(c);
    //Genoma mejor_8(c);

    // Búsquedas locales en los 8 intervalos
    // Generación de casos para GRASP

    /*gs_1.run_grid();
    mejor_1 = gs_1.get_parametrizacion();
    cerr << "mejor_1";
    gs_1.print_genoma(mejor_1);
    cerr << "---------------------------------------------" << endl;

    gs_2.run_grid();
    mejor_2 = gs_2.get_parametrizacion();
    cerr << "mejor_2";
    gs_2.print_genoma(mejor_2);
    cerr << "---------------------------------------------" << endl;

    gs_3.run_grid();
    mejor_3 = gs_3.get_parametrizacion();
    cerr << "mejor_3";
    gs_3.print_genoma(mejor_3);
    cerr << "---------------------------------------------" << endl;

    gs_4.run_grid();
    mejor_4 = gs_4.get_parametrizacion();
    cerr << "mejor_4";
    gs_4.print_genoma(mejor_4);
    cerr << "---------------------------------------------" << endl;

    gs_5.run_grid();
    mejor_5 = gs_5.get_parametrizacion();
    cerr << "mejor_5";
    gs_5.print_genoma(mejor_5);
    cerr << "---------------------------------------------" << endl;

    gs_6.run_grid();
    mejor_6 = gs_6.get_parametrizacion();
    cerr << "mejor_6";
    gs_6.print_genoma(mejor_6);
    cerr << "---------------------------------------------" << endl;

    gs_7.run_grid();
    mejor_7 = gs_7.get_parametrizacion();
    cerr << "mejor_7";
    gs_7.print_genoma(mejor_7);
    cerr << "---------------------------------------------" << endl;

    gs_8.run_grid();
    mejor_8 = gs_8.get_parametrizacion();
    cerr << "mejor_8";
    gs_8.print_genoma(mejor_8);
    cerr << "---------------------------------------------" << endl;*/


    // Selecciono al mejor de los 8
    // Fase de selección de GRASP

    vector<Genoma> participantes(8, Genoma(7, 0));
    //participantes.at(0) = {-0.7, -0.72, -0.9, -0.84, -0.94, -0.9, -0.84, -0.9, -0.9, -0.9};
    //participantes.at(1) = {-0.75, -0.55, -0.55, -0.55, -0.55, -0.55, -0.55, -0.55, -0.55, -0.55};
    //participantes.at(2) = {-0.5, -0.3, -0.5, -0.5, -0.3, -0.3, -0.5, -0.3, -0.3, -0.3};
    //participantes.at(3) = {-0.25, -0.25, -0.25, -0.05, -0.05, -0.05, -0.05, -0.05, -0.05, -0.05};
    //participantes.at(4) = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    //participantes.at(5) = {0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25};
    //participantes.at(6) = {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
    //participantes.at(7) = {0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75};

    //Optimizados y random
    participantes.at(0) = {-0.7, -0.72, -0.9, -0.84, -0.94, -0.9, -0.84, -0.9, -0.9, -0.9};
    participantes.at(1) = {-0.765, -0.545, -0.565, -0.565, -0.545, -0.545, -0.545, -0.545, -0.545, -0.545};
    participantes.at(2) = {-0.495, -0.295, -0.495, -0.495, -0.295, -0.295, -0.495, -0.495, -0.495, -0.495};
    participantes.at(3) = {-0.265, -0.265, -0.265, -0.045, -0.045, -0.045, -0.045, -0.045, -0.045, -0.045};
    participantes.at(4) = {0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25};
    //Random
    participantes.at(5) = {-0.765, -0.545, -0.565, -0.045, -0.045, -0.045, -0.045, -0.45, -0.015, 0.005};
    participantes.at(6) = {-0.265, -0.265, -0.265, -0.84, -0.84, -0.94, -0.9, -0.84, -0.9, -0.9, -0.9};
    participantes.at(7) = {0.005, -0.545, -0.9, -0.565, -0.495, -0.295, -0.295, -0.495, -0.495, -0.495, -0.495};

    //Incluir participantes random en lugar de los peores

    GridSearch gs_final = GridSearch(-1, 1, gra, c, 0.02, 0.015);

    cerr << "Comienzo del GRASP" << endl;

    for (int k = 0; k < participantes.size(); k++) {
      cerr << "Participante " << k << ": ";
      gs_final.print_genoma(participantes.at(k));
    }
    // Nueva búsqueda local
    // Fase de optimización del mejor de GRASP
    for (int i = 0; i < corridas_grasp; i++) {

      for (int j = 5; j < 8; j++) {
        gs_final.optimizar_param(participantes.at(j));
        cerr << "Llegué" << endl;

        Genoma opt = gs_final.get_parametrizacion();

        participantes.at(j) = opt;
        cerr << "Participante " << j << " optimizado: ";
        gs_final.print_genoma(opt);
      }
      //Obtengo el nuevo mejor
      gs_final.torneo(participantes);

      Genoma opt = gs_final.get_parametrizacion();
      cerr << "Mejor obtenido por GRASP: ";
      gs_final.print_genoma(opt);
    }
    return 0;
}
