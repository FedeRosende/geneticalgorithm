/*----------------------------------------------------------------------------*/
/* BIBLIOTECAS                                                                */
/*----------------------------------------------------------------------------*/

#include "grid_search.hpp"
#include "../Equipo.h"
#include "../Tablero.h"

/*----------------------------------------------------------------------------*/
/* DEFINICIONES DE CLASES                                                     */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* CLASE | GridSearch                                                         */
/*----------------------------------------------------------------------------*/

void GridSearch::run_grid () {
    Genoma actual = Genoma(_cps);

    _mejor.assign(_cps, _min);

    run_rec(actual, _cps-1);
}

// Se obtiene la mejor parametrización
Genoma &GridSearch::get_parametrizacion () {
    return _mejor;
}

// Paso recursivo de la metaheurística
void GridSearch::run_rec (Genoma &actual, int k) {
    // No hay mas parámetros que definir
    cerr << "actual ";
    print_genoma(actual);
    if(k < 0) {
        // Actualización de la mejor parametrización
        if (es_mejor(actual)) {
            cerr << "mejor hasta ahora ==== ";
            print_genoma(_mejor);
            _mejor.clear();
            _mejor.assign(actual.begin(), actual.end());
        }

        return;
    }
    // Hay parámetros por definir
    for( float i = _min; i <= _max; i = i + _gra_inicial ) {

        actual.at(k) = i;

        run_rec (actual, k-1);

    }
}

// Juega un torneo todos contra todos y asigna a _mejor el campeón
int GridSearch::torneo (vector<Genoma> &participantes){
  vector<int> puntajes(participantes.size(), 0);
  for (int i = 0; i < participantes.size()-1; i++) {
    for (int j = i+1; j < participantes.size(); j++) {
      cout << "i,j = " << i << ", " << j << endl;
      bool res = le_gana(participantes.at(i), participantes.at(j));
      if (res){
        puntajes.at(i) += 1;
        puntajes.at(j) -= 1;
      } else {
        puntajes.at(j) += 1;
        puntajes.at(i) -= 1;
      }
    }
  }
  for (int i = 0; i < puntajes.size(); i++) {
    cout << "puntaje del genoma " << i << "  " << puntajes.at(i) << endl;
  }
  int max_pos_punt = max_pos(puntajes);
  for (int i = 0; i < 6; i++) {
    cerr << "Ganador del torneo: ";
    print_genoma(participantes.at(max_pos_punt));
    puntajes.at(max_pos_punt) = -50;

  }
  _mejor.assign(participantes.at(max_pos_punt).begin(), participantes.at(max_pos_punt).end());

  return min_pos(puntajes);
}

void GridSearch::optimizar_param(Genoma & param) {
  vector<float> rangos(20, 0);
  // Correspondencias de máximos y mínimos en relación con el valor de _mejor.at(i)
  // 0-1: 0, 2-3: 1, 4-5: 2, 6-7: 3, 8-9: 4, 10-11: 5, 12-13: 6

  rangos.at(0) = (param.at(0)-_offset) < -1 ? -1 : (param.at(0)-_offset);
  rangos.at(1) = (param.at(0)+_offset) >  1 ? 1 : (param.at(0)+_offset);

  rangos.at(2) = (param.at(1)-_offset) < -1 ? -1 : (param.at(1)-_offset);
  rangos.at(3) = (param.at(1)+_offset) >  1 ? 1 : (param.at(1)+_offset);

  rangos.at(4) = (param.at(2)-_offset) < -1 ? -1 : (param.at(2)-_offset);
  rangos.at(5) = (param.at(2)+_offset) >  1 ?  1 : (param.at(2)+_offset);

  rangos.at(6) = (param.at(3)-_offset) < -1 ? -1 : (param.at(3)-_offset);
  rangos.at(7) = (param.at(3)+_offset) >  1 ?  1 : (param.at(3)+_offset);

  rangos.at(8) = (param.at(4)-_offset) < -1 ? -1 : (param.at(4)-_offset);
  rangos.at(9) = (param.at(4)+_offset) >  1 ?  1 : (param.at(4)+_offset);

  rangos.at(10) = (param.at(5)-_offset) < -1 ? -1 : (param.at(5)-_offset);
  rangos.at(11) = (param.at(5)+_offset) >  1 ?  1 : (param.at(5)+_offset);

  rangos.at(12) = (param.at(6)-_offset) < -1 ? -1 : (param.at(6)-_offset);
  rangos.at(13) = (param.at(6)+_offset) >  1 ?  1 : (param.at(6)+_offset);

  rangos.at(14) = (param.at(7)-_offset) < -1 ? -1 : (param.at(7)-_offset);
  rangos.at(15) = (param.at(7)+_offset) >  1 ?  1 : (param.at(7)+_offset);

  rangos.at(16) = (param.at(8)-_offset) < -1 ? -1 : (param.at(8)-_offset);
  rangos.at(17) = (param.at(8)+_offset) >  1 ?  1 : (param.at(8)+_offset);

  rangos.at(18) = (param.at(9)-_offset) < -1 ? -1 : (param.at(9)-_offset);
  rangos.at(19) = (param.at(9)+_offset) >  1 ?  1 : (param.at(9)+_offset);

  cerr << "Rangos: " << endl;
  print_genoma(rangos);
  cerr << "------------------------------------------------" << endl;

  Genoma actual(10, 0);
  actual = param;
  _mejor = param;
  run_rec_top(actual, _cps-1, rangos);
}

void GridSearch::run_rec_top(Genoma &actual, int k, vector<float> & rangos){
  cerr << "actual = ";
  print_genoma(actual);
  if(k < 0) {
      // Actualización de la mejor parametrización
      if (es_mejor(actual)) {
          _mejor.clear();
          _mejor.assign(actual.begin(), actual.end());
          cerr << "mejor hasta ahora ==== ";
          print_genoma(_mejor);
      }

      return;
  } else if (k == 0){
    for( float i = rangos.at(0); i <= rangos.at(1); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else if (k == 1) {
    for( float i = rangos.at(2); i <= rangos.at(3); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else if (k == 2){
    for( float i = rangos.at(4); i <= rangos.at(5); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else if (k == 3){
    for( float i = rangos.at(6); i <= rangos.at(7); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else if (k == 4) {
    for( float i = rangos.at(8); i <= rangos.at(9); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else if (k == 5){
    for( float i = rangos.at(10); i <= rangos.at(11); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  } else {
    for( float i = rangos.at(12); i <= rangos.at(13); i = i + _gra_final ) {

        actual.at(k) = i;

        run_rec_top (actual, k-1, rangos);

      }
  }
}

// Determina si la parametrización actual es mejor que la mejor parametrización
bool GridSearch::es_mejor (Genoma & actual) {
    //Juego 4 partidos entre el mejor hasta ahora y el actual
    //Devuelvo true si el actual ganó más de 2
    int ganados_actual = 0;
    pair<int,int> resultado;
    for (int i = 0; i < 2; i++) {
      resultado = competirDosGenomas(_mejor,actual);
      ganados_actual = resultado.first < resultado.second ? ganados_actual + 1 : ganados_actual;

      resultado = competirDosGenomas(actual,_mejor);
      ganados_actual = resultado.first > resultado.second ? ganados_actual + 1 : ganados_actual;
    }
    return ganados_actual>2;
}

// Determina si genoma_a es mejor que genoma_b
bool GridSearch::le_gana(Genoma & genoma_a, Genoma & genoma_b){
  cout << "Juego" << endl;
  int ganados_a = 0;
  pair<int,int> resultado;
  for (int i = 0; i < 2; i++) {
    resultado = competirDosGenomas(genoma_b,genoma_a);
    ganados_a = resultado.first < resultado.second ? ganados_a + 1 : ganados_a;

    resultado = competirDosGenomas(genoma_a,genoma_b);
    ganados_a = resultado.first > resultado.second ? ganados_a + 1 : ganados_a;
  }
  return ganados_a>2;
}

void GridSearch::print_genoma(Genoma & gen){
  cerr << "[";
  for (int i = 0; i < gen.size(); i++) {
    cerr << gen.at(i);
    if (i+1 < gen.size()){
      cerr << ", ";
    } else {
      cerr << "]";
    }
  }
  cerr << endl;
}

// Imprime la mejor parametrización
void GridSearch::print_top(){
  print_genoma(_mejor);
}

// Asigna la mejor parametrización
void GridSearch::asingar_top(Genoma nuevo_mejor){
  _mejor.assign(nuevo_mejor.begin(), nuevo_mejor.end());
}

// Devuelve la posición del máximo
int max_pos(vector<int> & resultados){
  int max_pos_actual = 0;
  for (int i = 1; i < resultados.size()-1; i++) {
    max_pos_actual = resultados.at(max_pos_actual) < resultados.at(i) ? i : max_pos_actual;
  }
  return max_pos_actual;
}

int min_pos(vector<int> & resultados){
  int min_pos_actual = 0;
  for (int i = 1; i < resultados.size()-1; i++) {
    min_pos_actual = resultados.at(min_pos_actual) > resultados.at(i) ? i : min_pos_actual;
  }
  return min_pos_actual;
}
// Hace jugar a dos genomas y devuelve el resultado
pair<int,int> competirDosGenomas(vector<float> & genomaA, vector<float> & genomaB){
    Equipo EquipoA = Equipo(genomaA, true,5,10,50);
    Equipo EquipoB = Equipo(genomaB, false,5,10,50);
    int Msobre2 = 5/2;
    int Nsobre2 = 10/2;
    Pelota pelota = Pelota(Nsobre2,Msobre2);
    Tablero tablero = Tablero(EquipoA, EquipoB,pelota, 10,5,50 );
    while(tablero.turnosRestantes() > 0){
        tablero.pasarTurno();
    }
    return tablero.resultado();
}
