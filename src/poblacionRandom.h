#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <iostream>

using namespace std;

//tamaNIo
vector<vector<float> > poblacionRandom(int tamanio, int cantGenes){
    srand(time(NULL));
    vector<vector<float> > res;
	for(int i=0; i < tamanio; i++){
	    vector<float> genoma;
        for(int j=0; j < cantGenes; j++){
            genoma.push_back(((float(rand()) / float(RAND_MAX)) *2) - 1);
        }
        res.push_back(genoma);
	}
    return res;
}

void guardarVector(vector<float> v, string s){
    FILE *pFile;
    pFile = fopen(s.c_str(), "w");
	for(int i=0; i < v.size(); i++){
		fprintf(pFile, "%f ", v[i]);
	}
	fclose (pFile);
}

void guardarPoblacion(vector<vector<float> > v, string s){
    FILE *pFile;
    pFile = fopen(s.c_str(), "w");
	for(int i=0; i < v.size(); i++){
		for(int j=0; j < v[i].size(); j++){		
			fprintf(pFile, "%f ", v[i][j]);
		}
		fprintf(pFile, "\n");
	}
	fclose (pFile);
}


