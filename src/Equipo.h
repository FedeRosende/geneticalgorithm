#ifndef SRC_EQUIPO_H
#define SRC_EQUIPO_H

#include <vector>

using namespace std;


struct Movimiento {
    Movimiento(bool patada, pair<int, int> direccion, int fuerza) :
            esPatada(patada), direccion(direccion), fuerza(fuerza) {};

    bool esPatada; //1 si es patada, 0 si es movimiento
    pair<int, int> direccion;
    int fuerza;
};


class Pelota {
public:
    Pelota(int posX, int posY) :
            _posX(posX), _posY(posY), _old_posX(posX), _old_posY(posY) {};

    void moverPelota(pair<int, int> direccion, int fuerza);

    void pasarTurno();

    void retrocederTurno();

    int turnosRestantes() {
        return _turnos_restantes;
    }

    pair<int, int> posicionFinal();

    void deshacerJugada();

    void confirmarMovimiento();

    //OJO las posiciones son (Y,X) porque son (fila, columna)
    pair<int, int> posicionActual();

    pair<int, int> direccionActual();

    void asignarPosicion(int posY, int posX);

private:
    int _posX; //columna actual
    int _posY; //fila actual
    int _old_posX;
    int _old_posY;
    int _turnos_restantes;
    int _old_turnos_restantes;
    pair<int, int> _direccion;
    pair<int,int> _old_direccion;
};


class Jugador {
public:
    Jugador(int id, float quite, int posY, int posX):
            _id(id), _proba_quite(quite), _posX(posX), _posY(posY), _old_posX(posX), _old_posY(posY), _tiene_pelota(false){};

    void hacerJugada(Pelota& pelota, const Movimiento& movimiento);

    void deshacerJugada(Pelota& pelota);

    void confirmarMovimiento(Pelota& pelota);

    void agarrarPelota();

    void perderPelota();

    void moverJugador(pair<int, int> direccion);

    void patear(Pelota& pelota, pair<int, int> direccion, int fuerza);

    int dameId() {
        return _id;
    }

    bool tienePelota() {
        return _tiene_pelota;
    }

    float probaQuite() {
        return _proba_quite;
    }

    int posX() {
        return _posX;
    }

    int posY() {
        return _posY;
    }

    void asignarPosicion(int posY, int posX);


private:
    bool _tiene_pelota;
    bool _old_tiene_pelota;
    int _posX;
    int _posY;
    int _old_posY;
    int _old_posX;
    int _id;
    float _proba_quite;
};


class Equipo {
public:
    Equipo(const vector<float> genoma, bool esjugadorA, int cant_filas, int cant_columnas, int turnos) {
        _genoma = genoma;
        _es_jugador_A = esjugadorA;
        _cant_filas = cant_filas;
        _cant_columnas = cant_columnas;
        _turnos_restantes = turnos;
        //Inicializamos las posiciones Iniciales recontra hardcodeadas
        //Arrancan los tres en la fila en el medio de la cancha(si son los que sacan)
        //con los tres del otro equipo  enfrente
        int mitadCanchaMenos1 = _cant_columnas/2 - _es_jugador_A + !_es_jugador_A;
        //es mitad cancha + 1 si es el B
        //supongo el A es del arco de la izquierda (columna 0) y B el de la derecha (col cant_colums)
        for(int i = 0; i < 3; i++){
            _pos_iniciales_cuando_no_saca.push_back(make_pair(_cant_filas/2 + i, mitadCanchaMenos1));
            _pos_iniciales_cuando_saca.push_back(make_pair(_cant_filas/2 + i, cant_columnas/2));
        }
        //Inicializamos los jugadores
        if(_es_jugador_A){
            for (int i = 0; i < 3; i++) {
                int posY = _pos_iniciales_cuando_saca[i].first;
                int posX = _pos_iniciales_cuando_saca[i].second;
                _jugadores.push_back(Jugador(i, modulo(genoma[i]), posY, posX));
            }
        }else{
             for (int i = 0; i < 3; i++) {
                int posY = _pos_iniciales_cuando_no_saca[i].first;
                int posX = _pos_iniciales_cuando_no_saca[i].second;
                _jugadores.push_back(Jugador(i, modulo(genoma[i]), posY, posX));
            }
        }
    }

    bool noPisoAOtro(Jugador& jugador, const Movimiento& movimiento);

    bool mover(Pelota& pelota, vector<Jugador>& oponentes);

    float evaluarTablero(Pelota pelota, vector<Jugador>& oponentes); //Hay que implementar esto

    bool esMovimientoValido(Jugador& jugador, const Movimiento& movimiento);

    vector<Jugador> jugadores();

    void inicializarJugadores(bool sale_jugando);

    float dispersionDeJugadores();

    void moverOponente(Pelota &pelota,vector<Jugador> &oponente);

    float modulo(float x){
        if(x < (float)0){
            x = x * (float)-1;
        }
        return x;
    }

    vector<Jugador> _jugadores;
    vector<pair<int,int> > _pos_iniciales_cuando_saca;
    vector<pair<int,int> > _pos_iniciales_cuando_no_saca;
    bool _es_jugador_A;
    int _cant_filas; //m cantidad de filas, n cantidad de columnas
    int _cant_columnas;
    vector<float> _genoma;
    int _turnos_restantes;
};


#endif //SRC_EQUIPO_H
