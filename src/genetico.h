#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <algorithm>
#include <queue>
#include <functional>

#include "poblacionRandom.h"
#include "Tablero.h"



//devuelve el marcador del partido
pair<int,int> competirDosGenomas(vector<float> &genomaA, vector<float> &genomaB){
    Equipo EquipoA = Equipo(genomaA, true,5,10,25);
    Equipo EquipoB = Equipo(genomaB, false,5,10,25);
    int Msobre2 = 5/2;
    int Nsobre2 = 10/2;
    Pelota pelota = Pelota(Nsobre2,Msobre2);
    Tablero tablero = Tablero(EquipoA, EquipoB,pelota, 10,5,25 );
    while(tablero.turnosRestantes() > 0){
        tablero.pasarTurno();
    }
    return tablero.resultado();
}




bool mayor(pair<int, vector<float> *> A, pair<int, vector<float> *> B){
    if(A.first > B.first){
        return true;
    }else{
        return false;
    }
}

float nroAleatorio(float Min, float Max) {
    return ((float(rand()) / float(RAND_MAX)) * (Max - Min)) + Min;
}

class compare{
    public:
        bool operator()(vector<float> &GenomaA, vector<float> &GenomaB){
            pair<int, int> ida = competirDosGenomas(GenomaA, GenomaB);
            pair<int, int> vuelta = competirDosGenomas(GenomaB, GenomaA);
            ida.first += vuelta.second;
            ida.second += vuelta.first;
            if (ida.first > ida.second){
                return true;
            }
            return false;
        }
};


//FITNESS
//COMPETIR TODOS CON TODOS OPTIMIZADO
vector<pair<int, vector<float> *> > competirTodosConTodos(vector<vector<float> > &poblacion){
    vector<pair<int, vector<float> *> > puntajes;
    for (int i = 0; i<poblacion.size(); i++){
        puntajes.push_back(make_pair(0, &poblacion[i]));
    }
    for (int i = 0; i<poblacion.size(); i++){
        //j arranca en i para no repetir partidos
        //en i + 1 en realidad, Un genoma no juega contra si mismo
        for(int j =i+1; j < poblacion.size(); j++){
            pair<int, int> ida = competirDosGenomas(poblacion[i],  poblacion[j]);
            pair<int, int> vuelta = competirDosGenomas(poblacion[j],  poblacion[i]);
            ida.first += vuelta.second; //ida.first goles que hizo el equipo i
            ida.second += vuelta.first; //goles que recibio el equipo i
            if (ida.second < ida.first){
                //El i gano los tres puntos
                puntajes[i].first +=3;
            }else if (ida.first == ida.second){
                //si es empate suma 1
                puntajes[i].first +=1;
                puntajes[j].first +=1;
            }else{
                //El j gano los tres puntos
                puntajes[j].first +=3;
            }
        }
    }
    return puntajes;
}

//FITNESS TORNEO
vector<vector<float> > torneo(vector<vector<float> > &poblacion, int limite){
    vector<vector<float> > puestos;

    //cola de prioridad que usa la funcion comparar
    std::priority_queue <vector<float>, vector<vector<float> >, compare> torneo;
    for (int i = 0; i<poblacion.size(); i++){
        torneo.push(poblacion[i]);
    }
    int poblacion_size = poblacion.size();
    for (int i = 0; i<limite; i++){
        puestos.push_back(torneo.top());
        torneo.pop();
    }

    return puestos;
}

//SELECTION MEJOR MITAD CON FITNESS TORNEO
void selectionTorneo(vector<vector<float> >  &poblacion){
    vector<vector<float> > puestos;

    puestos = torneo(poblacion,poblacion.size()/2);


    //Se eliminan los peores Equipos de poblacion
    vector<vector<float> > supervivientes;
    for(int i = 0; i<(poblacion.size()/2); i++){
        supervivientes.push_back(puestos[i]);
    }

    poblacion = supervivientes;
}



//SELECTION MEJOR MITAD
void selection(vector<vector<float> >  &poblacion){
    vector<pair<int, vector<float> *> > puntajes;

    puntajes = competirTodosConTodos(poblacion);

    //Se ordena de mayor a menor
    std::sort(puntajes.begin(), puntajes.end(), mayor);

    //Se eliminan los peores Equipos de poblacion
    vector<vector<float> > supervivientes;
    for(int i = 0; i<(puntajes.size()/2); i++){
        supervivientes.push_back(*(puntajes[i].second));
    }

    poblacion = supervivientes;
}

//SELECTION SOLO SALVO AL MEJOR CON FITNES TORNEO
void selectionMejorTorneo(vector<vector<float> >  &poblacion){
    vector<vector<float> > puestos;

    puestos = torneo(poblacion,1);
    vector<float> mejor = puestos[0];

    //desordeno a todos
    random_shuffle( poblacion.begin(), poblacion.end() );

    //podria incluir al mejor dos veces

    vector<vector<float> > supervivientes;
    supervivientes.push_back(mejor);
    for(int i = 1; i<(poblacion.size()/2); i++){
        supervivientes.push_back(poblacion[i]);
    }

    poblacion = supervivientes;
}

//SELECTION SOLO SALVO AL MEJOR
void selectionMejor(vector<vector<float> >  &poblacion){
    vector<pair<int, vector<float> *> > puntajes;

    puntajes = competirTodosConTodos(poblacion);

    //Se ordena de mayor a menor
    sort(puntajes.begin(), puntajes.end(), mayor);

    auto it = puntajes.begin();
    it++;

    //desordeno a todos menos el primero
    random_shuffle( it, puntajes.end() );


    vector<vector<float> > supervivientes;
    for(int i = 0; i<(puntajes.size()/2); i++){
        supervivientes.push_back(*(puntajes[i].second));
    }

    poblacion = supervivientes;
}



//CROSSOVER Y MUTACION JUNTOS
//Crossover en un punto random
//Cada gen j de cada hijo tiene una posibilidad de mutar
//muta en un numero aleatoria entre -1 y 1
void crossoverYMutacion(vector<vector<float> >  &poblacion, int proba_mutar){
    int genoma_size = poblacion[0].size();
    int cant_padres = poblacion.size();
    for(int i = 0; i<cant_padres; i = i+2){
        //genero nro random que va a servir como tope para el crossover
        int cross_point = rand() % genoma_size;
        //agrego dos hijos a la poblacion intercambiando/mutando los genes de menor indice que cross_point
        vector<float>  padre1 = poblacion[i];
        vector<float>  padre2 = poblacion[i+1];
        for(int j = 0; j <= cross_point; j++){
            //los cross_point ultimos del hijo1 son los cross_point ultimos del progenitor 2
            padre1[genoma_size -1 -j] = poblacion[i+1][genoma_size -1 -j];
            //los cross_point ultimos del hijo2 son los cross_point ultimos del progenitor 1
            padre2[genoma_size -1 -j] = poblacion[i][genoma_size -1 -j];
        }
        for(int j = 0; j < genoma_size; j++){
            if((float)rand() / RAND_MAX <= (float)proba_mutar/100){
                padre1[j] = nroAleatorio(-1,1);
            }
            if((float)rand() / RAND_MAX <= (float)proba_mutar/100){
                padre2[j] = nroAleatorio(-1,1);
            }
        }
        poblacion.push_back(padre1);
        poblacion.push_back(padre2);
    }
}

//CROSSOVER Y MUTACION JUNTOS
//Crossover es el promedio de los valores de los padres
//Cada gen j de cada hijo tiene una posibilidad de mutar
//muta en un numero aleatoria entre -1 y 1
void crossoverPromedioYMutacion(vector<vector<float> >  &poblacion, int proba_mutar){

    int genoma_size = poblacion[0].size();
    int cant_padres = poblacion.size();
    for(int i = 0; i<cant_padres; i = i+2){
        vector<float>  padre1 = poblacion[i];
        vector<float>  padre2 = poblacion[i+1];
        for(int j = 0; j < genoma_size; j++){
            float promedio = (poblacion[i][j] + poblacion[i+1][j])/2;
            if((float)rand() / RAND_MAX > (float)proba_mutar/100){               
                padre1[j] = promedio;
            }else{
                padre1[j] = nroAleatorio(-1,1);
            }
            if((float)rand() / RAND_MAX > (float)proba_mutar/100){              
                padre2[j] = promedio;
            }else{
                padre2[j] = nroAleatorio(-1,1);
            }
        }
        poblacion.push_back(padre1);
        poblacion.push_back(padre2);
    }
}



//Ve el genoma promedio solo para las generaciones viejas
//es una forma de ver cuanto esta variando la mejor mitad de una
//poblacion a otra
vector<float> genoma_promedio (vector<vector<float> >  &poblacion){
    int genoma_size = poblacion[0].size();
    vector<float> genoma_promedio(genoma_size);
    for (int i = 0; i<poblacion.size()/2; i++){
        for (int j = 0; j<genoma_size; j++){
            if(j==0 | j == 1 | j ==2){
                genoma_promedio[j] += abs(poblacion[i][j]);
            }else{
                genoma_promedio[j] += poblacion[i][j];
            }
        }
    }
    for (int j = 0; j<genoma_size; j++){
        genoma_promedio[j] = genoma_promedio[j]/(poblacion.size()/2);
    }

    return genoma_promedio;
}

bool similares (vector<float> &a, vector<float> &b){
    for (int j = 0; j<a.size(); j++){
        if (a[j] - b[j] > 0.1 or a[j] - b[j] < -0.1){
            return false;
        }
    }
    return true;
}

//OBS: ES EL DESVIO ESTANDAR DE LA MEJOR MITAD!
float desviacionEstandarPromedio(vector<vector<float> > & poblacion){
    //vamos a calcular el desvio estandar de cada gen y despues ver un promedio
    //empezamos viendo el promedio por cada gen

    int genoma_size = poblacion[0].size();
    vector<float> promedioPorGen(genoma_size);
    for (int i = 0; i<poblacion.size()/2; i++){
        for (int j = 0; j<genoma_size; j++){
            if(j==0 || j == 1 || j ==2){
                promedioPorGen[j] += abs(poblacion[i][j]);
            }else{
                promedioPorGen[j] += poblacion[i][j];
            }
        }
    }
    for (int j = 0; j<genoma_size; j++){
        promedioPorGen[j] = promedioPorGen[j]/(poblacion.size()/2);
    }
    //ahora vamos a calcular un vector donde en cada posicion tiene el desvio estandar
    //de la poblacion en el gen correspondiente a dicha posicion
    vector<float> desvioPorGen(genoma_size);
    for (int i = 0; i<poblacion.size()/2; i++){
        for (int j = 0; j<genoma_size; j++){
            if(j==0 || j == 1 || j ==2){
                desvioPorGen[j] += pow(abs(poblacion[i][j]) - promedioPorGen[j],2);
            }else{
                desvioPorGen[j] += pow(poblacion[i][j] - promedioPorGen[j],2);
            }
        }
    }
    for (int j = 0; j<genoma_size; j++){
        desvioPorGen[j] = sqrt(desvioPorGen[j]/((poblacion.size()/2)-1));
    }
    //vemos el promedio del desvio estandar
    float prom;
    for (int j = 0; j<genoma_size; j++){
        prom += desvioPorGen[j];
    }
    return prom/genoma_size;
}


void evolucionPoblacion(vector<vector<float> > & poblacion){
    FILE *pf;
    pf = fopen("evolucion.csv", "a");
    vector<float> pin_pon;
    pin_pon.push_back((float)1/2);
    pin_pon.push_back((float)1/2);
    pin_pon.push_back((float)1/2);
    pin_pon.push_back((float)-1);
    pin_pon.push_back((float)-1);
    pin_pon.push_back((float)1);
    pin_pon.push_back((float)-1);
    pin_pon.push_back((float)-1);
    pin_pon.push_back((float)1);
    pin_pon.push_back((float)-1);
    int puntaje = 0;
    //int goles_a_favor = 0;
    //int goles_en_contra = 0;
    for(int i = 0; i < poblacion.size(); i++){
	    //pin pon juega 10 partidos con poblacion[i], 5 de cada lado
	    //se suman los goles a favor de pin pon y los goles en contra en esos partidos
	    //se promedia ese resultado y se suman a goles_a_favor y goles_en_contra
	    int ganados = 0;

        //pin pon juega 5 partidos con poblacion[i], cada partido en verdad son
        //dos partidos (uno de ida y otro de vuelta para que jeugue en ambos lados de la cancha)
        //si pin pon gana mas de 3 partidos suma uno en puntaje
        
        pair<int,int> ida = competirDosGenomas(pin_pon,poblacion[i]);
        pair<int,int> vuelta= competirDosGenomas(poblacion[i],pin_pon);
        //en todos ida.first := goles que hizo pin pon en ambos partidos
        //ida.second := goles que hizo poblacion[i] en ambos aprtidos
        ida.first = ida.first + vuelta.second;
        ida.second = ida.second + vuelta.first;
        if(ida.first > ida.second){
            ganados++;
        }

        ida = competirDosGenomas(pin_pon,poblacion[i]);
        vuelta= competirDosGenomas(poblacion[i],pin_pon);
        ida.first = ida.first + vuelta.second;
        ida.second = ida.second + vuelta.first;
        if(ida.first > ida.second){
            ganados++;
        }

        ida = competirDosGenomas(pin_pon,poblacion[i]);
        vuelta= competirDosGenomas(poblacion[i],pin_pon);
        ida.first = ida.first + vuelta.second;
        ida.second = ida.second + vuelta.first;
        if(ida.first > ida.second){
            ganados++;
        }
        
        ida = competirDosGenomas(pin_pon,poblacion[i]);
        vuelta= competirDosGenomas(poblacion[i],pin_pon);
        ida.first = ida.first + vuelta.second;
        ida.second = ida.second + vuelta.first;
        if(ida.first > ida.second){
            ganados++;
        }

        ida = competirDosGenomas(pin_pon,poblacion[i]);
        vuelta= competirDosGenomas(poblacion[i],pin_pon);
        ida.first = ida.first + vuelta.second;
        ida.second = ida.second + vuelta.first;
        if(ida.first > ida.second){
            ganados++;
        }

        if(ganados >= 3){
            puntaje++;
        }
        
        //if((ida.first + vuelta.second) > (ida.second + vuelta.first)){
        //    puntaje = puntaje + 3;
        //}
        //if((ida.first + vuelta.second) == (ida.second + vuelta.first)){
        //    puntaje = puntaje +1;
        //}
    }
    //puntaje = goles_a_favor - goles_en_contra;
    fprintf(pf, "%d", puntaje);
    fprintf(pf, "\n");
    fclose(pf);
}


vector<float>  genetico(vector<vector<float> > & poblacion, int proba_mutar){
    //inicializamos semilla random
    srand(time(NULL));
    FILE *pFile;
    pFile = fopen("desvioEstandar.csv", "w");
    //Le agrego un limite porque tarda un huevo
    int limite = 50;
    int i = 0;

    //mientras no convergen los genomas, hago
    int converge = 0;
    vector <float> promedio_antiguo = genoma_promedio(poblacion);
    vector <float> promedio_nuevo;
    while (converge < 5 and i < limite){
        //vamos a imprimir en un archivo los desvios estandar promedios de las poblaciones
        float desvioPromedio = desviacionEstandarPromedio(poblacion);
        fprintf(pFile, "%f ", desvioPromedio);
        fprintf(pFile, "\n");
        evolucionPoblacion(poblacion);
        //SELECIONAR MEJOR MITAD CON FITNES TODOS CONTRA TODOS
        selection(poblacion);
        //CROSSOVER EN PUNTO DE CORTE RANDOM
        crossoverYMutacion(poblacion, proba_mutar);
        promedio_nuevo = genoma_promedio(poblacion);
        if(similares(promedio_antiguo, promedio_nuevo)){
            converge++;
        }else{
            converge = 0;
            promedio_antiguo = promedio_nuevo;
        }
        cout << "vamos por la poblacion: " << i+1 << endl; //la 0 es la inicial
        i = i+1;

    }
    //guardamos el desvio Promedio de la poblacion final
    float desvioPromedio = desviacionEstandarPromedio(poblacion);
    fprintf(pFile, "%f ", desvioPromedio);
    fclose (pFile);
    evolucionPoblacion(poblacion);
    //guardamos la poblacion final
    guardarPoblacion(poblacion,"poblacion_final.csv");
    return poblacion[0];
}

