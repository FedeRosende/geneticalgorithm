#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <algorithm>
#include <queue>
#include <functional>

#include "genetico.h"

int main() {
    vector<float> genomaA;
    genomaA.push_back((float)1/2);
    genomaA.push_back((float)1/2);
    genomaA.push_back((float)1/2);
    genomaA.push_back((float)1/5);
    genomaA.push_back((float)1/7);
    genomaA.push_back((float)-1);
    genomaA.push_back((float)1/4);
    genomaA.push_back((float)1/5);
    genomaA.push_back((float)1/5);
    genomaA.push_back((float)1/5);

    vector<float> genomaB;
    genomaB.push_back((float)1/2);
    genomaB.push_back((float)1/2);
    genomaB.push_back((float)1/2);
    genomaB.push_back((float)-1/5);
    genomaB.push_back((float)-1/7);
    genomaB.push_back((float)1);
    genomaB.push_back((float)-1/4);
    genomaB.push_back((float)-1/5);
    genomaA.push_back((float)1/4);
    genomaA.push_back((float)1/4);
    genomaA.push_back((float)1/4);



    //creo poblacion random
    vector<vector<float> > poblacion;
    poblacion = poblacionRandom(8,10);

    guardarPoblacion(poblacion,"poblacion_inicial.csv");

    cout << "la desviacion promedio es: " << desviacionEstandarPromedio(poblacion) << endl;

	//CALCULO EL GENETICO
    int proba_mutar = 5; //El valor es el porcentaje de probabilidad de mutar, proba_mutar = 5 => 5% probabilidad
	vector<float> equipoGenetico = genetico(poblacion, proba_mutar);
    //Muestor el genetico
    for(int i=0; i < equipoGenetico.size(); i++){
        cout << equipoGenetico[i] << endl;
    }
    //comparo el genetico con el genomaA que es cualquier cosa
    //el genetico tendria que ganar las dos, si no es una cagada
    pair<int,int> resultado;
    resultado = competirDosGenomas(genomaA,equipoGenetico);
    //resultado = competirDosGenomas(genomaA,poblacion[0]);
    if(resultado.first > resultado.second){
        cout << "GANO A" << endl;
    }else if(resultado.first == resultado.second){
        cout << "EMPATE" << endl;
    }else{
        cout << "GANO equipoGenetico" << endl;
    }
    resultado = competirDosGenomas(equipoGenetico,genomaA);
    //resultado = competirDosGenomas(poblacion[0],genomaA);
    if(resultado.first > resultado.second){
        cout << "GANO equipoGenetico" << endl;
    }else if(resultado.first == resultado.second){
        cout << "EMPATE" << endl;
    }else{
        cout << "GANO A" << endl;
    }
    return 0;
}
