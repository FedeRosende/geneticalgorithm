#ifndef SRC_TABLERO_H
#define SRC_TABLERO_H

#include <vector>
#include "Equipo.h"
#include <iostream>
#include <time.h>
using namespace std;

class Tablero {
public:
    Tablero(Equipo &A, Equipo &B, Pelota pelota, int n, int m, int turnos):
            _equipoA(A), _equipoB(B), _pelota(pelota),
            _cant_filas(m), _cant_columnas(n), _turnos_restantes(turnos) {
            //le ponemos la pelota al jugador [0] de A
            //sabemos que nuestros equipos inicializan dicho jugador
            //en la posición (M/2 , N/2) (cuando son equipo A)
            _equipoA._jugadores[0].agarrarPelota();
            _pelota_libre = false;
            _puntaje = make_pair(0,0);
            //cout << "INICIANDO UN PARTIDO" << endl;
            //imprimirTablero();
            srand(time(NULL)); //inicializamos semilla random
            }; //m cantidad de filas, n cantidad de columnas

    void pasarTurno();

    int turnosRestantes();

    pair<int,int> resultado();

    void moverPelota();

    void disputarPelota(int jugadorA , int jugadorB);

    void imprimirTablero();

    bool equipoATienePelota();

private:
    Equipo _equipoA;
    Equipo _equipoB;
    Pelota _pelota;
    pair<int, int> _puntaje;
    int _cant_filas; //m cantidad de filas, n cantidad de columnas
    int _cant_columnas;
    bool _pelota_libre;
    int _turnos_restantes;
};


#endif //SRC_TABLERO_H
