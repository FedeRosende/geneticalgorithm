#include "Tablero.h"
#include <vector>
#include <stdlib.h>

using namespace std;

void Tablero::disputarPelota(int jugadorA , int jugadorB){
    //observar que se tiene que fijar si A o B tienen la pelota
    //porque el disputar no es igual si la pelota esta libre
    //y los dos pelean en igualdad de condiciones
    //o si uno tiene la pelota y tiene que manterla
    float p_quiteA = _equipoA._jugadores[jugadorA].probaQuite();
    float p_quiteB = _equipoB._jugadores[jugadorB].probaQuite();
    if((_equipoA._jugadores[jugadorA]).tienePelota()){
        float X = ((float) rand() / (RAND_MAX)); //numero random entre 0 y 1
        if(X <= p_quiteB/(p_quiteB + (1 - p_quiteA))){
            //el jugador B le quita la pelota al A
            _equipoA._jugadores[jugadorA].perderPelota();
            _equipoB._jugadores[jugadorB].agarrarPelota();
        }
        return;
    }
    if((_equipoB._jugadores[jugadorB]).tienePelota()){
        float X = ((float) rand() / (RAND_MAX)); //numero random entre 0 y 1
        if(X <= p_quiteA/(p_quiteA + (1 - p_quiteB))){
            //el jugador A le quita la pelota al B
            _equipoB._jugadores[jugadorB].perderPelota();
            _equipoA._jugadores[jugadorA].agarrarPelota();
        }
        return;
    }
    //ninguno de los dos tiene la pelota
    float X = ((float) rand() / (RAND_MAX)); //numero random entre 0 y 1
    if(X <= p_quiteA/(p_quiteA + p_quiteB)){
        //el jugador A gana la pelota
        _equipoA._jugadores[jugadorA].agarrarPelota();
        _pelota_libre = false; //nadie tenia la pelota
        //detenemos la pelota
        _pelota.moverPelota(make_pair(0,0),0);
        _pelota.asignarPosicion(_equipoA._jugadores[jugadorA].posY(), _equipoA._jugadores[jugadorA].posX());
        _pelota.confirmarMovimiento();
    }else{
        //el jugador B gana la pelota
        _equipoB._jugadores[jugadorB].agarrarPelota();
        _pelota_libre = false; //nadie tenia la pelota
        //detenemos la pelota
        _pelota.moverPelota(make_pair(0,0),0);
        _pelota.asignarPosicion(_equipoB._jugadores[jugadorB].posY(), _equipoB._jugadores[jugadorB].posX());
        _pelota.confirmarMovimiento();
    }
    return;
}

void Tablero::moverPelota(){
    pair<int, int> posPelota = _pelota.posicionActual();
    pair<int, int> dirPelota = _pelota.direccionActual();
    int interceptorA = -1;
    int interceptorB = -1;
    vector<Jugador> jugadoresA = _equipoA.jugadores();
    vector<Jugador> jugadoresB = _equipoB.jugadores();
    //vemos si hay alguno en A que pueda interceptar la pelota
    posPelota.first += dirPelota.first;
    posPelota.second += dirPelota.second;
    for(int i = 0; i< 3; i++){
        if(jugadoresA[i].posX() == posPelota.second and jugadoresA[i].posY() == posPelota.first){
            interceptorA = i;
        }
        if(jugadoresB[i].posX() == posPelota.second and jugadoresB[i].posY() == posPelota.first){
            interceptorB = i;
        }
    }
    if(interceptorA >=0){
        if(interceptorB >=0){
            this -> disputarPelota(interceptorA,interceptorB);
        }else{
            _equipoA._jugadores[interceptorA].agarrarPelota();
            _pelota_libre = false;
            //detenemos la pelota
            _pelota.moverPelota(make_pair(0,0),0);
            _pelota.asignarPosicion(_equipoA._jugadores[interceptorA].posY(), _equipoA._jugadores[interceptorA].posX());
            _pelota.confirmarMovimiento();
        }
    }else{
        if(interceptorB >=0){
            _equipoB._jugadores[interceptorB].agarrarPelota();
            _pelota_libre = false;
            //detenemos la pelota
            _pelota.moverPelota(make_pair(0,0),0);
            _pelota.asignarPosicion(_equipoB._jugadores[interceptorB].posY(), _equipoB._jugadores[interceptorB].posX());
            _pelota.confirmarMovimiento();
        }else{
            //no hay nadie en el medio, puede todavia haber alguien
            //en la posicion a la quese meuve la pelota al apsar el turno
            interceptorA = -1;
            interceptorB = -1;
            posPelota.first += dirPelota.first;
            posPelota.second += dirPelota.second;
            for(int i = 0; i< 3; i++){
                if(jugadoresA[i].posX() == posPelota.second and jugadoresA[i].posY() == posPelota.first){
                    interceptorA = i;
                }
                if(jugadoresB[i].posX() == posPelota.second and jugadoresB[i].posY() == posPelota.first){
                    interceptorB = i;
                }
            }
            if(interceptorA >=0){
                if(interceptorB >=0){
                    this -> disputarPelota(interceptorA,interceptorB);
                }else{
                    _equipoA._jugadores[interceptorA].agarrarPelota();
                    _pelota_libre = false;
                    //detenemos la pelota
                    _pelota.moverPelota(make_pair(0,0),0);
                    _pelota.asignarPosicion(_equipoA._jugadores[interceptorA].posY(),
                                            _equipoA._jugadores[interceptorA].posX());
                    _pelota.confirmarMovimiento();
                }
            }else{
                if(interceptorB >=0){
                    _equipoB._jugadores[interceptorB].agarrarPelota();
                    _pelota_libre = false;
                    //detenemos la pelota
                    _pelota.moverPelota(make_pair(0,0),0);
                    _pelota.asignarPosicion(_equipoB._jugadores[interceptorB].posY(),
                                            _equipoB._jugadores[interceptorB].posX());
                    _pelota.confirmarMovimiento();
                }else{
                    //no la intercepta nadie, movemos la pelota
                    _pelota.pasarTurno();
                    _pelota.confirmarMovimiento();
                }
            }
        }
    }
}

bool Tablero::equipoATienePelota(){
    for(int i=0; i<3; i++){
        if(_equipoA._jugadores[i].tienePelota()){
            return true;
        }
    }
    return false;
}

void Tablero::pasarTurno(){
    //cout << " " << endl;
    //cout << "NUEVO TURNO" << endl;
    vector<Jugador> jugadoresA = _equipoA.jugadores();
    vector<Jugador> jugadoresB = _equipoB.jugadores();
    //mover devuelve true si en los movimientos del equipo se incluye una patada
    //entonces la pelota queda libre
    Pelota pelotaVieja = _pelota;
    if(_equipoA.mover(_pelota, jugadoresB)){
	//	cout << "El jugador A pateo la pelota" << endl;
        _pelota_libre = true;
    }
    //PARA QUE EL EQUIPO B NO JUEGUE CON VENTAJA LE PASAMOS UNA COPIA VIEJA DE LOS JUGADORES DE A
    //Y TAMBIEN SI EL NO TIENE LA PELOTA (ENTONCES SU MOVIMIENTO NO PUEDE MODIFICAR SU POSICION
    //AL MENOS QUE LA INTERCEPTE PERO DE ESO SE ENCARGA EL TABLERO) LE PASAMOS UNA COPIA DE LA PELOTA
    //PREVIA AL MOVIMIENTO DE A
    if(equipoATienePelota()){
        _equipoB.mover(pelotaVieja,jugadoresA);
        //observar que si el B no tiene la pelota entonces tampoco puede cambiar
        //si la misma esta libre o no
    }else if(_equipoB.mover(_pelota, jugadoresA)){
	//	cout << "El jugador B pateo la pelota" << endl;
        _pelota_libre = true;
    }
    if(_pelota_libre){
        this -> moverPelota();
    }else{
        //me fijo si no hay dos en el mismo casillero con la pelota
        //observar que hay al menos uno y ese tiene la pelota
        //pues la misma no esta libre
        int disputandoA = -1;
        int disputandoB = -1;

        for(int i = 0; i< 3; i++){
            if(_equipoA._jugadores[i].posX() == _pelota.posicionActual().second and
                    _equipoA._jugadores[i].posY() == _pelota.posicionActual().first){
                disputandoA = i;
            }
            if(_equipoB._jugadores[i].posX() == _pelota.posicionActual().second and
                    _equipoB._jugadores[i].posY() == _pelota.posicionActual().first){
                disputandoB = i;
            }
        }
        if(disputandoA >=0){
            if(disputandoB >=0){
                this -> disputarPelota(disputandoA, disputandoB);
            }
        }
    }
    //supongo que se puede meter un jugador en el arco con la pelota y todo
    //FIJARSE SI SE HUBO UN GOL
    //pos_pelota = (pos Y , pos X)
    //posY = fila, posX = columna
    pair<int,int> pos_pelota = _pelota.posicionActual();
    if(pos_pelota.second == -1){
        //La pelota esta en el arco del jugador A
        //Observar que el arco esta fuera de la cancha
        //y la única forma de salir de la cancha es estar
        //en el arco, entonces no nos tenemos que fijar
        //en que fila esta la pelota
        _puntaje.second = _puntaje.second + 1;
        //inicializamos jugadores de A, dado que arrancan
        //jugando
        _equipoA.inicializarJugadores(true);
        _equipoB.inicializarJugadores(false);
        _pelota_libre = false;
        _pelota.asignarPosicion(_cant_filas / 2, _cant_columnas / 2);
        _pelota.confirmarMovimiento();
    }
    if(pos_pelota.second == _cant_columnas){
        _puntaje.first = _puntaje.first +1;
        _equipoA.inicializarJugadores(false);
        _equipoB.inicializarJugadores(true);
        _pelota_libre = false;
        _pelota.asignarPosicion(_cant_filas / 2, _cant_columnas / 2);
        _pelota.confirmarMovimiento();
    }
    _turnos_restantes --;
    _equipoA._turnos_restantes--;
    _equipoB._turnos_restantes--;
    //Esto lo estoy usando para testear
    //this -> imprimirTablero();
    //int a;
    //cin >> a;
}


void Tablero::imprimirTablero(){
    if(true){
        pair<int,int> posPELOTA = _pelota.posicionActual();
        cout << "POS PELOTA: " << posPELOTA.first << " " << posPELOTA.second << endl;
    }
    cout << " Pos jugadores A: " << endl;
    for(int i = 0; i<3 ; i++){
        if(_equipoA._jugadores[i].tienePelota()){
            cout << "Tengo la pelota" << endl;
        }
        cout << _equipoA._jugadores[i].posY();
        cout << " " <<  _equipoA._jugadores[i].posX() << endl;
    }
    cout << " Pos jugadores B: " << endl;
    for(int i = 0; i<3 ; i++){
        if(_equipoB._jugadores[i].tienePelota()){
            cout << "Tengo la pelota" << endl;
        }
        cout << _equipoB._jugadores[i].posY();
        cout << " " <<  _equipoB._jugadores[i].posX() << endl;
    }
    cout << "Puntaje: " << _puntaje.first << " " << _puntaje.second << endl;
}


int Tablero::turnosRestantes(){
    return _turnos_restantes;
}

pair<int,int> Tablero::resultado(){
    return _puntaje;
}
