#include "Equipo.h"
#include <tuple>
#include <limits>
#include "Utils.h"
#include "iostream"
using namespace std;

void Pelota::moverPelota(pair<int, int> direccion, int fuerza) {
    _direccion = direccion;
    _turnos_restantes = fuerza;
}

void Pelota::pasarTurno() {
    if(_turnos_restantes >0){
        _posY += 2*_direccion.first;
        _posX += 2*_direccion.second;
        _turnos_restantes--;
    }else{
        _direccion = make_pair(0,0);
    }
}

//OJO las posiciones son (Y,X) porque son (fila, columna)
pair<int, int> Pelota::posicionActual() {
    return make_pair(_posY, _posX);
}

pair <int,int> Pelota::posicionFinal(){
    int posX = _posX + _turnos_restantes*2*_direccion.second;
    int posY = _posY + _turnos_restantes*2*_direccion.first;
    return make_pair(posY,posX);
}

pair<int, int> Pelota::direccionActual(){
    pair<int,int> res = _direccion;
    return res;
}

void Pelota::deshacerJugada() {
    _posX = _old_posX;
    _posY = _old_posY;
    _direccion = _old_direccion;
    _turnos_restantes = _old_turnos_restantes;
}

//Retroceder turno es distinto a deshacerJugada, pues no cambia la dirección
void Pelota::retrocederTurno(){
    _posY -= 2*_direccion.first;
    _posX -= 2*_direccion.second;
    _turnos_restantes++;
}

void Pelota::confirmarMovimiento() {
    _old_posX = _posX;
    _old_posY = _posY;
    _old_direccion = _direccion;
    _old_turnos_restantes = _turnos_restantes;
}
void Pelota::asignarPosicion(int posY, int posX){
    _posX = posX;
    _posY = posY;
}

void Jugador::moverJugador(pair<int, int> direccion) {
    _posY += direccion.first;
    _posX += direccion.second;
}

void Jugador::agarrarPelota() {
    _tiene_pelota = true;
    _old_tiene_pelota = true;
}

void Jugador::perderPelota() {
    _tiene_pelota  = false;
    _old_tiene_pelota = false;
}

void Jugador::patear(Pelota& pelota, pair<int, int> direccion, int fuerza) {
    pelota.moverPelota(direccion, fuerza);
    //pelota.pasarTurno();
    _tiene_pelota = false;
}

void Jugador::deshacerJugada(Pelota &pelota) {
    _posX = _old_posX;
    _posY = _old_posY;
    _tiene_pelota = _old_tiene_pelota;
    if(_old_tiene_pelota){
        pelota.deshacerJugada();
    }
}

void Jugador::confirmarMovimiento(Pelota &pelota) {
    _old_posX = _posX;
    _old_posY = _posY;
    _old_tiene_pelota = _tiene_pelota;
    if(this -> tienePelota()){
        pelota.confirmarMovimiento();
    }
}

void Jugador::hacerJugada(Pelota &pelota, const Movimiento &movimiento) {
    if (movimiento.esPatada) {
        patear(pelota, movimiento.direccion, movimiento.fuerza);
    } else {
        moverJugador(movimiento.direccion);
        if(this -> tienePelota()){
            pelota.asignarPosicion(this->posY(), this->posX());
        }
    }
}

void Jugador::asignarPosicion(int posY, int posX){
    _old_posX = posX;
    _old_posY = posY;
    _posX = posX;
    _posY = posY;
}


vector<Jugador> Equipo::jugadores(){
    return _jugadores;
}


void Equipo::moverOponente(Pelota &pelota,vector<Jugador> &oponentes){
    Equipo otroEquipo(_genoma,!_es_jugador_A,_cant_filas,_cant_columnas,_turnos_restantes);
    otroEquipo._jugadores = oponentes;
    //Movemos otroEquipo suponiendo que el rival (nuestro equipo) se queda quieto
    vector<vector<Movimiento> > movimientos_posibles_por_jugador(3); //Para cada jugador genero un vector con todos los movimientos posibles
    for (int jugador = 0; jugador < 3; jugador++) {
        for (int i = -1; i <= 1; i++) { //Para cada direccion posible
            for (int j = -1; j <= 1; j++) {
                pair<int,int> direccion = make_pair(i, j);
                for (int pateo = 0; pateo <= 1; pateo++) { //Considero si es una patada o un movimiento
                    Movimiento movimiento_actual = Movimiento(false, direccion, 0);
                    if (pateo and otroEquipo._jugadores[jugador].tienePelota()) {
                        for (int fuerza = 1; fuerza <= _cant_columnas / 4; fuerza++) {
                            movimiento_actual.esPatada = true;
                            movimiento_actual.fuerza = fuerza;
                            if(otroEquipo.esMovimientoValido(otroEquipo._jugadores[jugador], movimiento_actual)){
                                movimientos_posibles_por_jugador[jugador].push_back(movimiento_actual);
                            }
                        }
                    }else{
                        if(!pateo){
                            if (otroEquipo.esMovimientoValido(otroEquipo._jugadores[jugador], movimiento_actual)) {
                                movimientos_posibles_por_jugador[jugador].push_back(movimiento_actual);
                            }
                        }
                    }
                }
            }
        }
    }

    //Ahora combino todos los movimientos posibles segun los movimientos de cada jugador
    vector<vector<Movimiento> > movimientos_posibles_equipo;
    for (int j0 = 0; j0 < movimientos_posibles_por_jugador[0].size(); j0++) {
        for (int j1 = 0; j1 < movimientos_posibles_por_jugador[1].size(); j1++) {
            for (int j2 = 0; j2 < movimientos_posibles_por_jugador[2].size(); j2++) {
                movimientos_posibles_equipo.push_back({
                        movimientos_posibles_por_jugador[0][j0],
                        movimientos_posibles_por_jugador[1][j1],
                        movimientos_posibles_por_jugador[2][j2]
                                                              });
            }
        }
    }

    //Evaluo cada movimiento posible del equipo y me quedo con el maximo
    float beneficio_maximo = ((float)-1)*numeric_limits<float>::max();
    vector<Movimiento> mejor_movimiento_equipo = movimientos_posibles_equipo[0];
    for (int i = 0; i < movimientos_posibles_equipo.size(); i++) {
        //Ejecuto los movimientos
        for (int j = 0; j < _jugadores.size(); j++) {
            //Si podes hacer el movimiento lo haces , sino te quedas quieto
            if(otroEquipo.noPisoAOtro(otroEquipo._jugadores[j], movimientos_posibles_equipo[i][j])){
                otroEquipo._jugadores[j].hacerJugada(pelota, movimientos_posibles_equipo[i][j]);
            }
        }
        //Evaluo el tablero resultante
        float beneficio_actual = otroEquipo.evaluarTablero(pelota, _jugadores);
        if (beneficio_actual > beneficio_maximo) {
            beneficio_maximo = beneficio_actual;
            mejor_movimiento_equipo = movimientos_posibles_equipo[i];
        }
        //Deshago los movimientos para seguir probando
        for (int j = 0; j < _jugadores.size(); j++) {
            otroEquipo._jugadores[j].deshacerJugada(pelota);
        }
    }
    //Confirmo el mejor movimiento encontrado
    for (int j = 0; j < 3; j++) {
        if(otroEquipo.noPisoAOtro(otroEquipo._jugadores[j],mejor_movimiento_equipo[j])){
            otroEquipo._jugadores[j].hacerJugada(pelota, mejor_movimiento_equipo[j]);
            otroEquipo._jugadores[j].confirmarMovimiento(pelota);
        }
    }

    oponentes = otroEquipo._jugadores;
}

//mover devuelve true si hubo una patada(si alguien dejo la pelota)
bool Equipo::mover(Pelota &pelota, vector<Jugador> &oponentes) {
    Pelota pelotaCopia = pelota;
    moverOponente(pelotaCopia, oponentes);
    //Si mi equipo NO tiene la pelota voy a llamar a evaluar tablero con pelotaCopia
    //observar que si yo no tengo la pelota no la puedo modificar en este turno
    //al menos que la intercepte, pero de eso se encarga el tablero.
    bool miEquipoTienePelota = false;
    for(int i=0; i<3; i++){
        if(_jugadores[i].tienePelota()){
            miEquipoTienePelota = true;
        }
    }
    //cout << " PREDICCIONES " << endl;
    //cout << oponentes[0].posY() << " " << oponentes[0].posX() << endl;
    //cout << oponentes[1].posY() << " " << oponentes[1].posX() << endl;
    //cout << oponentes[2].posY() << " " << oponentes[2].posX() << endl;

    vector<vector<Movimiento> > movimientos_posibles_por_jugador(3); //Para cada jugador genero un vector con todos los movimientos posibles
    for (int jugador = 0; jugador < 3; jugador++) {
        for (int i = -1; i <= 1; i++) { //Para cada direccion posible
            for (int j = -1; j <= 1; j++) {
                pair<int,int> direccion = make_pair(i, j);
                for (int pateo = 0; pateo <= 1; pateo++) { //Considero si es una patada o un movimiento
                    Movimiento movimiento_actual = Movimiento(false, direccion, 0);
                    if (pateo and _jugadores[jugador].tienePelota()) {
                        for (int fuerza = 1; fuerza <= _cant_columnas / 4; fuerza++) {
                            movimiento_actual.esPatada = true;
                            movimiento_actual.fuerza = fuerza;
                            if(esMovimientoValido(_jugadores[jugador], movimiento_actual)){
                                movimientos_posibles_por_jugador[jugador].push_back(movimiento_actual);
                            }
                        }
                    }else{
                        if(!pateo){
                            if (esMovimientoValido(_jugadores[jugador], movimiento_actual)) {
                                movimientos_posibles_por_jugador[jugador].push_back(movimiento_actual);
                            }
                        }
                    }
                }
            }
        }
    }

    //Ahora combino todos los movimientos posibles segun los movimientos de cada jugador
    vector<vector<Movimiento> > movimientos_posibles_equipo;
    for (int j0 = 0; j0 < movimientos_posibles_por_jugador[0].size(); j0++) {
        for (int j1 = 0; j1 < movimientos_posibles_por_jugador[1].size(); j1++) {
            for (int j2 = 0; j2 < movimientos_posibles_por_jugador[2].size(); j2++) {
                movimientos_posibles_equipo.push_back({
                        movimientos_posibles_por_jugador[0][j0],
                        movimientos_posibles_por_jugador[1][j1],
                        movimientos_posibles_por_jugador[2][j2]
                                                              });
            }
        }
    }

    //Evaluo cada movimiento posible del equipo y me quedo con el maximo
    float beneficio_maximo = ((float)-1)*numeric_limits<float>::max();
    vector<Movimiento> mejor_movimiento_equipo = movimientos_posibles_equipo[0];
    for (int i = 0; i < movimientos_posibles_equipo.size(); i++) {
        //Ejecuto los movimientos
        for (int j = 0; j < _jugadores.size(); j++) {
            //Si podes hacer el movimiento lo haces , sino te quedas quieto
            if(noPisoAOtro(_jugadores[j], movimientos_posibles_equipo[i][j])){
                _jugadores[j].hacerJugada(pelota, movimientos_posibles_equipo[i][j]);
            }
        }
        //Evaluo el tablero resultante
        float beneficio_actual;
        if(miEquipoTienePelota){
            beneficio_actual = evaluarTablero(pelota, oponentes);
        }else{
            beneficio_actual = evaluarTablero(pelotaCopia, oponentes);
        }
        if (beneficio_actual > beneficio_maximo) {
            beneficio_maximo = beneficio_actual;
            mejor_movimiento_equipo = movimientos_posibles_equipo[i];
        }
        //Deshago los movimientos para seguir probando
        for (int j = 0; j < _jugadores.size(); j++) {
            _jugadores[j].deshacerJugada(pelota);
        }
    }
    //Confirmo el mejor movimiento encontrado

    bool hayPatada = false;
    for (int j = 0; j < _jugadores.size(); j++) {
        if(noPisoAOtro(_jugadores[j],mejor_movimiento_equipo[j])){
            _jugadores[j].hacerJugada(pelota, mejor_movimiento_equipo[j]);
            _jugadores[j].confirmarMovimiento(pelota);

            if(mejor_movimiento_equipo[j].esPatada){
                hayPatada = true;
                //pelota.retrocederTurno();
            }
        }
    }
    pelota.confirmarMovimiento();
    return hayPatada;
}

void Equipo::inicializarJugadores(bool sale_jugando){
    if(sale_jugando){
        for (int i = 0; i < 3; i++) {
            int posY = _pos_iniciales_cuando_saca[i].first;
            int posX = _pos_iniciales_cuando_saca[i].second;
            _jugadores[i].asignarPosicion(posY, posX);
            _jugadores[i].perderPelota();
        }
        //en un principio le voy a poner la pelota aca al jugador del medio
        //al que esta en la posicion (M/2, N/2)
        //Observar entonces que cuando creas un tablero el mismo se inicializa
        //con pelota_libre en false y la pelota se inicializa en esa pos inicial
        _jugadores[0].agarrarPelota();
    }else{
        for (int i = 0; i < 3; i++) {
            int posY = _pos_iniciales_cuando_no_saca[i].first;
            int posX = _pos_iniciales_cuando_no_saca[i].second;
            _jugadores[i].asignarPosicion(posY, posX);
            _jugadores[i].perderPelota();
        }
    }
}

bool Equipo::noPisoAOtro(Jugador &jugador, const Movimiento &movimiento){
    //Tengo que ver no haya otro jugador de mi equipo en esa posicion
    int pos_final_x;
    int pos_final_y;
    pos_final_x = jugador.posX() +  movimiento.direccion.second;
    pos_final_y = jugador.posY() +  movimiento.direccion.first;

    if(movimiento.esPatada){
        return true;
    }
    for(int i=0; i < 3; i++){
        if(i != jugador.dameId()){
            if(pos_final_x == _jugadores[i].posX() and pos_final_y == _jugadores[i].posY()){
                return false;
            }
        }
    }
    return true;
}

bool Equipo::esMovimientoValido(Jugador &jugador, const Movimiento &movimiento) {
    int pos_final_x;
    int pos_final_y;
    if (movimiento.esPatada) {
        pos_final_x = jugador.posX() + (movimiento.fuerza * 2) * movimiento.direccion.second;
        pos_final_y = jugador.posY() + (movimiento.fuerza * 2) * movimiento.direccion.first;
        //se puede patear atrás del arco y esa es una posición válida
        // si pos _final _ y (la fila donde esta la pelota) esta en alguna de las filas donde esta el arco
        //-> es válido porque incluso si se pasa de _cant_columnas el arco va interceptar la pelota
        if(pos_final_y == _cant_filas/2 - 1 || pos_final_y == _cant_filas/2 || pos_final_y == _cant_filas/2 + 1){
            return true;
        }
    } else {
        pos_final_x = jugador.posX() + movimiento.direccion.second;
        pos_final_y = jugador.posY() + movimiento.direccion.first;
        //El arco es una posición afuera de la cancha váldia (el jugador puede entrar al arco con la pelota)
        //si pos_final_x (columna) es - 1 o N y pos_final_y (fila) conincide con una de las filas del arco
        // -> es movimiento válido
        if(pos_final_x == -1 || pos_final_x == _cant_columnas){
            if(pos_final_y == _cant_filas/2 - 1 || pos_final_y == _cant_filas/2 || pos_final_y == _cant_filas/2 + 1){
               return true;
            }
        }
    }
    //por último si no entro en alguno de los otros casos, vemos que el movimiento este adentro de la cancha
    return pos_final_x < _cant_columnas and pos_final_y < _cant_filas and pos_final_x >= 0 and pos_final_y >= 0;
}

float Equipo::evaluarTablero(Pelota pelota, vector<Jugador> &oponentes) {
    vector<pair<int,int> > posiciones_arco_rival;
    //En vez de ver la posición final de la pleota lo que hacemos es mover la pelota hasta que algun rival la intercepte
    pair<int,int> posFinalPelota = pelota.posicionFinal();
    pair<int,int> posPelota = pelota.posicionActual();
    pair<int,int> direccionPelota= pelota.direccionActual();
    posFinalPelota.second +=1;
    posPelota.second +=1;
    if (pelota.turnosRestantes() != 0) { //Sin esta guarda se bugeaba
        posPelota.first += direccionPelota.first;
        posPelota.second += direccionPelota.second;
    }
    //posPelota va a ser hasta donde llega la pelota hasta que la interceptan
    //posPelotaProximoTurno es lo que su nombre indica
    pair<int,int> posPelotaProximoTurno = posPelota;
    bool interceptaron = false;
    while(posPelota.first != posFinalPelota.first
            and posPelota.second != posFinalPelota.second and !interceptaron){
        for(int i = 0; i< 3; i++){
            if(posPelota.first == oponentes[i].posY() and posPelota.second == oponentes[i].posX() + 1){
                interceptaron = true;
            }
        }
        if(!interceptaron){
            posPelota.first += direccionPelota.first;
            posPelota.second += direccionPelota.second;
        }
    }

    //Seteo la columna del arco rival
    int arco_pos_x;
    if (_es_jugador_A) {
        arco_pos_x = _cant_columnas+1;
    } else {
        arco_pos_x = 0;
    }
    //Seteo las filas del arco oponente
    for (int i = -1; i <= 1; i++) {
        int arco_pos_y = _cant_filas/2 + i;
        posiciones_arco_rival.push_back(make_pair(arco_pos_y, arco_pos_x));
    }
    //(Despues podriamos hacer un metodo para lo anterior)

    //Calculo la distancia de la pelota al arco
    float dist_min = distanciaEuclidea(posPelota,posiciones_arco_rival[0]);
    for (int i = 1; i < 3; i++) {
        if(dist_min > distanciaEuclidea(posPelota, posiciones_arco_rival[i])){
            dist_min = distanciaEuclidea(posPelota, posiciones_arco_rival[i]);
        }
    }

    //DISTANCIA MIN  DEL EQUIPO A LA PELOTA
    float dist_min_pelota = distanciaEuclidea(posPelota, make_pair(_jugadores[0].posY(),_jugadores[0].posX()+1));
    float dist_min_pelotaProximoTurno = distanciaEuclidea(posPelotaProximoTurno, make_pair(_jugadores[0].posY(),_jugadores[0].posX()+1));
    for (int i = 1; i < 3; i++) {
        if(dist_min_pelota > distanciaEuclidea(posPelota, make_pair(_jugadores[i].posY(),_jugadores[i].posX()+1))){
            dist_min_pelota = distanciaEuclidea(posPelota, make_pair(_jugadores[i].posY(),_jugadores[i].posX()+1));
        }
        if(dist_min_pelotaProximoTurno > distanciaEuclidea(posPelotaProximoTurno, make_pair(_jugadores[i].posY(),_jugadores[i].posX()+1))){
            dist_min_pelotaProximoTurno = distanciaEuclidea(posPelotaProximoTurno, make_pair(_jugadores[i].posY(),_jugadores[i].posX()+1));
        }
    }
    //EL JUGADOR QUE TIENE LA PELOTA NO ES ESTA PARADO EN LA MISMA POSICION QUE UN RIVAL
    bool estoyDisputandoPelota = false;
    bool equipo_tiene_pelota = false;
    for (int i = 0; i < 3; i++){
        if(_jugadores[i].tienePelota()){
            bool equipo_tiene_pelota = true;
            for(int j=0; j < 3; j++){
                if(oponentes[j].posX() == _jugadores[i].posX() and oponentes[j].posY() == _jugadores[i].posY()){
                    estoyDisputandoPelota = true;
                }
            }
        }
    }


    //DISPERSION
    float dispersion = dispersionDeJugadores();

    //DISTANCIA DEL EQUIPO AL EQUIPO CONTRARIO CUANDO TENEMOS Y CUANDO NO TENEMOS LA PELOTA
    float dist_entre_equipos = 0;
    for (int i = 0; i < oponentes.size(); i++) {
        float distancia_min = numeric_limits<float>::max();
        for (int j = 0; j < jugadores().size(); j++) {
            float dist_nueva = distanciaEuclidea(make_pair(_jugadores[j].posY(), _jugadores[j].posX()),
                                                 make_pair(oponentes[i].posY(), oponentes[i].posX()));
            if (distancia_min > dist_nueva) {
                distancia_min = dist_nueva;
            }
        }
        dist_entre_equipos += distancia_min;
    }
    dist_entre_equipos = dist_entre_equipos/3;

    float dist_entre_equipos_con_pelota = dist_entre_equipos * equipo_tiene_pelota;
    float dist_entre_equipos_sin_pelota = dist_entre_equipos * !equipo_tiene_pelota;

    //HAY QUE NORMALIZAR LAS DISTANCIAS
    //if(!_es_jugador_A){
    //cout << "la dist a la pelota min vale: " << dist_min_pelota << endl;
    //cout << "la dist min al arco rival es: " << dist_min << endl;
    //cout << "la dispersion mes: " << dispersion << endl;
    //cout << "El tablero vale: " << _genoma[3]*dist_min  + _genoma[4]*dist_min_pelota + _genoma[5] * dispersion << endl;
    //cout << _genoma[4] << endl;
    //}


    return _genoma[3]*dist_min  + _genoma[4]*dist_min_pelota + _genoma[5]*dispersion +
        _genoma[6]* estoyDisputandoPelota + _genoma[7]*dist_min_pelotaProximoTurno +
            _genoma[8]*dist_entre_equipos_con_pelota + _genoma[9]*dist_entre_equipos_sin_pelota;
}

float Equipo::dispersionDeJugadores() {
    float perimetro = 0;
    for (int i = 0; i < _jugadores.size(); i++) {
        auto pos1 = make_pair(_jugadores[i].posY(), _jugadores[i].posX());
        auto pos2 = make_pair(_jugadores[(i+1) % 3]. posY(), _jugadores[(i+1) % 3].posX());
        perimetro += distanciaEuclidea2(pos1, pos2)/3; //Normalizo a 1/3 asi entre todas suman 1
    }
    return perimetro;
}
