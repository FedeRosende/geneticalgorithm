#ifndef SRC_UTILS_H
#define SRC_UTILS_H

#include <cmath>
#include "Tablero.h"

using namespace std;

//Norma de un punto
float norma(pair<float,float> pair){
    float norm;
    norm = sqrt(pow(pair.first,2) + pow(pair.second,2));
    return norm;
}

//Funcion para calcular distancia euclidea
float distanciaEuclidea2(pair<float,float> pair1, pair<float, float> pair2) {
    float norm1 = norma(pair1);
    float norm2 = norma(pair2);
    pair1 = make_pair(pair1.first/norm1,pair1.second/norm1);
    pair2 = make_pair(pair2.first/norm2,pair2.second/norm2);
    return sqrt(pow(pair2.first - pair1.first, 2) +
                pow(pair2.second - pair1.second, 2));
}

float distanciaEuclidea(pair<float,float> pair1, pair<float, float> pair2) {
    return sqrt(pow(pair2.first - pair1.first, 2) +
                pow(pair2.second - pair1.second, 2));
}

#endif //SRC_UTILS_H
